## Description
The Grow Degree Calulator (GDC) package contains a command-line driven 
Python code, "gdc.py", which calculates Grow Degree Days (GDD); 
a handy value in agriculture used to predict maturity stages of 
crops and insects alike.

The "gdc.py" program currently uses Weather, Research, 
and Forecast (WRF) model data for daily maximum and minimum 
temperatures along with user supplied parameters. Users can specify 
location, run dates, lower and upper temperature limits. 

A GUI implementation of this package is on the Agrineer.org 
[website](https://agrineer.org/gdc/gdc.php) and covers about half the of the western United States, with a ~3km pixel resolution.

The package was developed using GNU/Linux Mint 17, but other Debian platforms are known to work (Ubuntu 12,14; Mint 18,19).

## Function
The Grow Degree Calculator (GDC) package implements a simple daily calculation:

                      (Daily Max Air Temp + Daily Min Air Temp)
    Daily Degree Day = ----------------------------------------  - Lower Temp
                                           2
    
Where:   
    
         "Daily Max Air Temp" is capped by the "Upper Temp" threshold.
    
and  
       
         if "Daily Degree Day" < 0 then set "Daily Degree Day" = 0.

Daily values are accumulated over a period of specified run days.

Users are encouraged to vary parameters in order to further the 
accuracies of this calculator as applied to local cultivars. 

## Current Implemention Stream
Modeled Weather-> Local/Crop parameters -> GDD calculations

This initial release uses reanalyzed Global Forecasting System (GFS) data as input to the WRF model to produce modeled, hindcast weather data; although other models and input data can be used, including forecast data.

The only data used from the WRF model are maximum and minimum temperatures for a specified location pixel.

## Motivation
The GDC was designed as an architectural preliminary exercise for 
the Soil Moisture Estimator package [SME](https://gitlab.com/agrineer/sme). 
It is suggested that programmers interested in the SME project 
should start with the GDC, as it gives a less complicated approach to 
the WRF netCDF4 data.

## Installation
### Dependencies
   - The [agrineer](https://gitlab.com/agrineer/agrineer)
     package must be installed.
   - WRF/ETo data files retrieved by programs in the "agrineer" package. 

### Setup
   Determine a directory to house the GDC project, eg. /home/user/projects.

   Change into this directory and either unpack or clone this project.  
   
         > cd /home/user/projects  
         
         > tar xvf gdc.tar
         or 
         > git clone https://gitlab.com/agrineer/gdc

### Running the gdc.py program

   The gdc.py program requires appropriate data and a configuration file. Data can be downloaded using programs in the "agrineer" package.

   You will need to create a "results" directory to direct gdc output into.  
   
          > mkdir somewhere/results

   An example configuration file is given in the gdc directory 
   called "gdc.conf". Copy this file, eg.

          > cp gdc.conf MyGdc.conf

   and modify the input values for your environment. Be sure to point to the newly made "results" directory to direct GDC output into.
   
   Details on input values can be found at our [wiki](https://www.agrineer.org/wiki/index.php?title=Grow_Degree_Calculator).
   
   Run:  
   
           > cd /home/agrineer/projects/gdc
           > ./gdc.py -c MyGdc.conf

   Look in the output directory specified in the configuration file for result output.
